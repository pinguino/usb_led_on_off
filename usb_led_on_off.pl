#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  usb_led_on.pl
#
#        USAGE:  ./usb_led_on.pl  
#
#  DESCRIPTION: Script para encender/apagar un led, conectado al pin 0 de un
#               dispisitivo Pingüino, enviando una señal por el puerto usb.
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#      LICENCE: This library is free software, you can redistribute it and/or
#               modify it under the same terms as Perl itself.
#       AUTHOR:  Nelo R. Tovar (), tovar.nelo en gmail @nelo en identi.ca
#      COMPANY:  Pinguino-Ve
#      VERSION:  1.0
#      CREATED:  18/09/11 16:36:43
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;
use Device::USB;
use Term::ReadLine;

my $usb = Device::USB->new() || die "No se puede inicializar el puerto USB!";
my $dev = $usb->find_device(0x04d8, 0xfeaa) || die "Pingüino no está conectado!";
$dev->open() || die " No se puede abrir la conexión con Pingüino!";
$dev->set_configuration(3);
$dev->claim_interface(0);

my $term = Term::ReadLine->new('Rectificar');
my $operacion = uc($term->readline('Opcion (E/A) > '));
$dev->bulk_write(0x01, $operacion, 100);

