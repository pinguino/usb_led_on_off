// Leer del USB y encender un led
#define LED 0			// led a encender
#define ENCENDER 'E'	// La letra 'E' para encender el led
#define APAGAR 'A'		// La letra 'A' para apagar el led

uchar accion;

void setup(void) {
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
}

void loop(void) {
  if (USB.available()) {	// ¿Hay datos en el buffer del USB?
    accion = USB.read();	// Leer el datos enviado
    switch (accion) {		// ejecutar la acción
	  case ENCENDER:
		digitalWrite(LED, HIGH);
		break;
	  case APAGAR:
		digitalWrite(LED, LOW);
		break;
	}
  }
}